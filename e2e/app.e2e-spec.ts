import { PMClient } from './app.po';

describe('MP App', () => {
  let page: PMClient;

  beforeEach(() => {
    page = new PMClient();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
