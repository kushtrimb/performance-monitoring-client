import {Injectable} from '@angular/core';

@Injectable()
export class Configuration {

  public Server = 'http://localhost:8080/';
  public ApiUri = 'metrics/';
  public Service = this.Server + this.ApiUri;
}
