import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MetricsService {

  constructor(private http: Http) {
  }

  getMetrics(firstRelease, secondRelease, srvNr) {
    return this.http.get('http://localhost:8080/metrics/diff/' + firstRelease + '/' + secondRelease + '/' + srvNr)
      .map((res: Response) => res.json());
  }

  getReleases() {
    return this.http.get('http://localhost:8080/releases')
      .map((res: Response) => res.json());
  }
}
