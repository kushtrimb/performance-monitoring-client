import {Component} from '@angular/core';
import {MetricsService} from './metricsService.component';

@Component({
  selector: 'app-server',
  templateUrl: './monitoring.component.html'
})
export class ServerComponent {
  metriCS = [];
  metrics = [];
  releases = [];
  srvNr = [{"id": 1, "name": "1"}, {"id": 2, "name": "2"}];
  metricPerUserNR = '';
  version = '';
  firstRelease = 0;
  secondRelease = 0;
  serversNr = 0;

  constructor(private metricsService: MetricsService) {
    this.metricsService.getReleases()
      .subscribe(data => this.releases = data);
  }

  loadMetrics() {

    this.metricsService.getMetrics(this.firstRelease, this.secondRelease, this.serversNr)
      .subscribe(data => this.metriCS = data)


    this.version = JSON.parse(JSON.stringify(this.metriCS)).firstVersion +
      " | " + JSON.parse(JSON.stringify(this.metriCS)).secondVersion;

    this.metrics = JSON.parse(JSON.stringify(this.metriCS)).comparisons;

    let mtr = [];

    for (let i = 0; i < JSON.parse(JSON.stringify(this.metriCS)).comparisons.length; i++) {
      let endpoint = JSON.parse(JSON.stringify(this.metriCS)).comparisons[i].endpoint;
      let avgDiff = JSON.parse(JSON.stringify(this.metriCS)).comparisons[i].avgDiff;
      let first = [" ", " ", " ", " ", " ", " ", " ", " "];

      for (let j = 0; j < JSON.parse(JSON.stringify(this.metriCS)).comparisons[i].metricPerUserNR.length; j++) {
        first[j] = JSON.parse(JSON.stringify(this.metriCS)).comparisons[i].metricPerUserNR[j].first
          + " | " + JSON.parse(JSON.stringify(this.metriCS)).comparisons[i].metricPerUserNR[j].second;

        mtr.push({
          "endpoint": endpoint,
          "first": first[0],
          "second": first[1],
          "third": first[2],
          "fourth": first[3],
          "fifth": first[4],
          "sixth": first[5],
          "seventh": first[6],
          "eighth": first[7],
          "diff": avgDiff
        })
      }
    }
    this.metrics = mtr;
    console.log(mtr);
  }
}
